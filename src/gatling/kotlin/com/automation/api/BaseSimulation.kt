package com.automation.api

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory

open class BaseSimulation() {

    //Constants
    private val applicationEnvProperty = "APPLICATION_ENV"
    private val executionModeProperty = "EXECUTION_MODE"
    private val applicationName = "api-test"

    //Loading environment variables
    private val generalConfig: Config = ConfigFactory.load()
    private val env = getValueFromConfig(applicationEnvProperty) ?: "ci"
    val executionMode = getValueFromConfig(executionModeProperty) ?: "load-single"
    var url=""
        private set
    var userEndpoint=""
        private set
    var testName=""
        private set
    var atOnceUserThreads = 0
        private set
    var rampUserThreads= 0
        private set
    var rampUserExecutionTime = 0.toLong()
        private set


    private fun getValueFromConfig(propertyName: String): String? {
        if (generalConfig.hasPath(propertyName))
            return generalConfig.getString(propertyName)
        return null
    }

    init {
        //Loading environment config file
        val applicationConfig: Config = ConfigFactory.load("application-${env.lowercase()}")
        url = applicationConfig.getString("$applicationName.url")
        userEndpoint= applicationConfig.getString("$applicationName.userEndpoint")
        testName= applicationConfig.getString("$applicationName.testName")
        atOnceUserThreads = applicationConfig.getInt("$applicationName.atOnceUserThreads")
        rampUserThreads= applicationConfig.getInt("$applicationName.rampUserThreads")
        rampUserExecutionTime =  applicationConfig.getLong("$applicationName.rampUserExecutionTime")
    }
}