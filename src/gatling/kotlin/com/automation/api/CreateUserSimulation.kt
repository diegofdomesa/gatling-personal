package com.automation.api

import io.gatling.javaapi.core.*
import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.http.HttpDsl.*
import io.gatling.javaapi.http.HttpProtocolBuilder

class CreateUserSimulation : Simulation() {

    //Load configuration
    private val baseSimulation = BaseSimulation()

    //Private util functions
    private fun randomUUID() = (10000..99999999).random().toString()

    //Request Body
    private val requestBodyCreateUser: String = """
    {
        "name": "#{name}_#{id}",
        "job": "leader"
    }
    """

    //Private util functions
    private val headers = mapOf("Content-Type" to "application/json")

    private val httpProtocol: HttpProtocolBuilder = http
        .baseUrl(baseSimulation.url)
        .headers(headers)

    //ChainBuilder
    private val generateUser: () -> ChainBuilder = {
        exec(
            http("post_create_user_#{type}")
                .post(baseSimulation.userEndpoint)
                .body(StringBody(requestBodyCreateUser)).asJson()
                .check(status().shouldBe(201))
                .check(jsonPath("$..name").exists())
                .check(jsonPath("$..job").exists())
                .check(jsonPath("$..id").exists())
                .check(jsonPath("$..createdAt").exists())
        )
    }

    //Scenarios
    private val generateSingleUser: ScenarioBuilder = scenario("Get placements for specific renderId")
        .exec { session ->
            val id = session.set("id", randomUUID())
            id
        }.exec { session ->
            val name = session.set("name", baseSimulation.testName)
            name
        }.exec{ session ->
            val type = session.set("type", "single")
            type
        }
        .exec(generateUser())

    // Injection of sessions //https://gatling.io/docs/gatling/reference/current/core/injection/
    init{
        when (baseSimulation.executionMode.lowercase()) {
            "load-single" ->
                setUp(
                    // Injects 5 users at the same time (for the same advertiser)
                    generateSingleUser.injectOpen(atOnceUsers(baseSimulation.atOnceUserThreads))
                ).protocols(httpProtocol)
            "scalability-single" ->
                setUp(
                    // Injects 5 users one by one during 20 seconds (for the same advertiser)
                    generateSingleUser.injectOpen(rampUsers(baseSimulation.rampUserThreads).during(baseSimulation.rampUserExecutionTime))
                ).protocols(httpProtocol)
            else ->
                setUp(
                    // Injects 5 users at the same time (for the same advertiser)
                    generateSingleUser.injectOpen(atOnceUsers(baseSimulation.atOnceUserThreads))
                ).protocols(httpProtocol)
        }
    }
}