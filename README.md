# Performance tests for REST API

## Prerequisites
* Gradle
* JDK 16

## Resources

* application-{env}.conf: config files that contain all the variable values per environment
* gatling.conf: Gatling configuration file. All the commented properties have the default value. If you need to customize one, uncomment it and change the value
* logback.xml: Log config file. Console is set as the default output

## Execution

### Environment variables:
* APPLICATION_ENV: this value will be used to select the config file (application-{env}.conf)
    * ci
    * prod
    * the default value is ci
* EXECUTION_MODE:
    * load-single: Injects 5 users at the same time (for the same name extracted from the config file)
    * scalability-single: Injects 5 users one by one during 20 seconds (for the same name extracted from the config file)
    * the default value is load-single

### Local Execution

* Specific class under src/gatling/kotlin

```
gradle gatlingRun-{package}.{className} -DAPPLICATION_ENV={env} -DEXECUTION_MODE={executionMode}

Example

gradle gatlingRun-com.automation.api.CreateUserSimulation -DAPPLICATION_ENV=ci -DEXECUTION_MODE=scalability-single
```
### Pipeline Execution

You can download the HTML report stored in the job's artifacts (the artifacts are available until 1 day ahead)

## Reports

After every execution, you can find the Gatling report in /build/reports/{classname}-{date}/index.html

